## this block is meant to set constraint on terraform version
terraform {
  required_version = ">= 1.1.0"

required_providers{
    aws = {
      source = "hashicorp/aws"
      version = "~> 4.0"
    }
}
}

## for now BEST
provider "aws" {
    region = "us-east-1"
   
  
}

## resource block (create resourse/bring into existence)
resource "aws_vpc" "main" {
    cidr_block       = "10.0.0.0/16"
    instance_tenancy = "default"

    tags = {
        name = "main"
    }
}

# how to ref a variable (var.ami_id) 


#creating ec2
resource "aws_instance" "ec2_instance" {
  ami           = var.ami_id
  instance_type = "t2.micro"

  tags = {
    Name = "ec2_instance"
  }
}

## data source pull down available resources in aws